﻿@tier2_quality = 2
@tier3_quality = 4

fallback_army_01 = {
	type = army
	entity = unit_western_infantry_01_entity
}

fallback_army_02 = {
	type = army
	entity = unit_western_infantry_02_entity
	quality_fallback = @tier2_quality
}

fallback_army_03 = {
	type = army
	entity = unit_western_infantry_03_entity
	quality_fallback = @tier3_quality
}

# Common Dwarf

common_dwarf_army_01 = {
	type = army
	graphical_cultures = { dwarven_unit_gfx }
	entity = unit_common_dwarf_infantry_01_entity
}

common_dwarf_army_02 = {
	type = army
	quality = @tier2_quality
	graphical_cultures = { dwarven_unit_gfx }
	entity = unit_common_dwarf_infantry_02_entity
}

common_dwarf_army_03 = {
	type = army
	quality = @tier3_quality 
	graphical_cultures = { dwarven_unit_gfx }
	entity = unit_common_dwarf_infantry_03_entity
}
