﻿anb_on_title_gained = {
	effect = {
		#Blademarches
		if = {
			limit = {
				scope:title = title:k_blademarches
			}
			if = {
				limit = {
					NOT = { has_character_modifier = wielded_calindal } # Don't do the trial on gaining the kingdom if you've already done the trial
				}
				trigger_event = { id = blademarches_succession.0001 days = 3 } # Right of the Blade
			}

			# Get a claim on CSalindal
			every_artifact = {
				limit = {
					has_variable = is_calindal
				}
				root = {
					add_personal_artifact_claim = prev
				}
			}

			# If the previous owner had Calindal it's yours now
			scope:previous_holder = {
				every_character_artifact = {
					limit = {
						has_variable = is_calindal
					}
					set_owner = root
				}
			}
		}

		#Rename the emperor of Castanor to Castan # TODO - Make this an event so players know what's happening
		if = {
			limit = { scope:title = title:e_castanor }
			change_first_name = Castan
		}
	}
}