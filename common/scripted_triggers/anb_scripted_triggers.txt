﻿#Anbennar - magical affinity

#Anbennar
has_magical_affinity_trigger = {
	has_trait = magical_affinity
}

has_skaldic_tale_active = {
	OR = {
		has_character_modifier = skaldhyrric_dirge
		has_character_modifier = skaldhyrric_gjalund
		has_character_modifier = skaldhyrric_beralic
		has_character_modifier = skaldhyrric_voyage
		has_character_modifier = skaldhyrric_golden_forest
		has_character_modifier = skaldhyrric_master_boatbuilders
		has_character_modifier = skaldhyrric_dragon_and_skald
		has_character_modifier = skaldhyrric_old_winter_lullaby
	}
}

can_move_title_into_gerudia_trigger = {
	trigger_if = {
		limit = {
			title_is_valid_to_move_to_gerudia = { TITLE = $TITLE$ }
		}
		completely_controls = $TITLE$
	}
	trigger_else = {
		# TODO - Add a tooltip that says "has not moved title to Gerudia"
		always = no
	}
}

title_is_valid_to_move_to_gerudia = {
	title_has_de_jure = { TITLE = $TITLE$ }
	$TITLE$ = {
		NOT = { empire = title:e_gerudia } 
		NOT = { has_variable = kingdom_moved_into_gerudia_empire }
	}
}

title_has_de_jure = {
	$TITLE$ = { any_in_de_jure_hierarchy = { tier = tier_county } }
}

anb_ai_wants_to_promote_culture = { # Anbennar TODO: keep this up-to-date
	OR = {
		AND = { # Redfoot and Bluefoot want to become the only haflings (but leave viswallers alone)
			OR = {
				scope:councillor_liege.culture = culture:bluefoot_halfling
				scope:councillor_liege.culture = culture:redfoot_halfling
			}
			OR = {
				scope:county.culture = culture:ciderfoot_halfling
				scope:county.culture = culture:roysfoot_halfling
				scope:county.culture = culture:oakfoot_halfling
				scope:county.culture = culture:beefoot_halfling
				scope:county.culture = culture:hillfoot_halfling
			}
		}
		# Elvenized Cultures
		AND = { # Lorentish want to convert Lorenti
			scope:councillor_liege.culture = culture:lorentish
			OR = {
				scope:county.culture = culture:lorenti
			}
		}
		AND = { # Damerian want to convert Old Damerian and Lenco-Damerian
			scope:councillor_liege.culture = culture:damerian
			OR = {
				scope:county.culture = culture:old_damerian
				scope:county.culture = culture:lenco_damerian
			}
		}
		AND = { # Esmari want to convert Old Esmari
			scope:councillor_liege.culture = culture:esmari
			OR = {
				scope:county.culture = culture:old_esmari
			}
		}
		AND = { # Sorncosti want to convert Sormanni
			scope:councillor_liege.culture = culture:sorncosti
			OR = {
				scope:county.culture = culture:sormanni
			}
		}
		AND = { # Vernman want to convert Vernid
			scope:councillor_liege.culture = culture:vernman
			OR = {
				scope:county.culture = culture:vernid
			}
		}
		AND = { # Castellyrian want to convert Castanorian
			scope:councillor_liege.culture = culture:castellyrian
			OR = {
				scope:county.culture = culture:castanorian
				scope:county.culture = culture:black_castanorian
			}
		}
		# Newer cultures want to convert old cultures
		AND = { # The Corvurians want to convert Kobarid
			scope:councillor_liege.culture = culture:corvurian
			OR = {
				scope:county.culture = culture:korbarid
			}
		}
		AND = { # Gawedi want to convert Old Alenic
			scope:councillor_liege.culture = culture:gawedi
			OR = {
				scope:county.culture = culture:old_alenic
			}
		}
		AND = { # Arannese want to convert Milcori and Roilsardi
			scope:councillor_liege.culture = culture:arannese
			OR = {
				scope:county.culture = culture:milcori
				scope:county.culture = culture:roilsardi
			}
		}
	}
}

anb_ai_wants_to_convert_province = {
	trigger_if = { # Elven Forebears converts only elven provinces
		limit = { scope:councillor.faith = faith:elven_forebears }
		scope:county = { is_elvish_culture = yes }
	}
	trigger_else = {
		always = yes
	}
}
