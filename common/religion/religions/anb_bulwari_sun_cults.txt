﻿bulwari_sun_cults_religion = {
	family = rf_bulwari
	graphical_faith = dharmic_gfx
	doctrine = sun_cult_hostility_doctrine
	
	doctrine = special_faith_dwarven_tolerance #All Bulwari faiths should have this, mostly so they don't constantly launch holy wars against Ovdal Tungr and the Seghbandal

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_polygamy
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_shunned

	#Clerical Functions
	doctrine = doctrine_clerical_function_taxation
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_cremation
	
	doctrine = special_doctrine_is_sun_cult_faith

	traits = {
		virtues = {
			honest
			gregarious
			patient
			lifestyle_gardener
		}
		sins = {
			deceitful
			callous
			impatient
			possessed
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_jaherian_exemplars" }
		{ name = "holy_order_faith_maharatas" }
	}

	holy_order_maa = { armored_footmen }	#todo

	localization = {
		#HighGod - Surakel
		HighGodName = bulwari_sun_cults_high_god_name
		HighGodName2 = bulwari_sun_cults_high_god_name
		HighGodNamePossessive = bulwari_sun_cults_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HERHIS_HIS
		HighGodHerHis = CHARACTER_HERHIM_HIM
		HighGodNameAlternate = bulwari_sun_cults_high_god_name_alternate
		HighGodNameAlternatePossessive = bulwari_sun_cults_high_god_name_alternate_possessive

		#Creator
		CreatorName = bulwari_sun_cults_creator_god_name
		CreatorNamePossessive = bulwari_sun_cults_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = bulwari_sun_cults_health_god_name
		HealthGodNamePossessive = bulwari_sun_cults_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = bulwari_sun_cults_fertility_god_name
		FertilityGodNamePossessive = bulwari_sun_cults_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_HE
		FertilityGodHerHis = CHARACTER_HERHIS_HIS
		FertilityGodHerHim = CHARACTER_HERHIM_HIM

		#WealthGod
		WealthGodName = bulwari_sun_cults_wealth_god_name
		WealthGodNamePossessive = bulwari_sun_cults_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = bulwari_sun_cults_household_god_name
		HouseholdGodNamePossessive = bulwari_sun_cults_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = bulwari_sun_cults_fate_god_name
		FateGodNamePossessive = bulwari_sun_cults_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = bulwari_sun_cults_knowledge_god_name
		KnowledgeGodNamePossessive = bulwari_sun_cults_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = bulwari_sun_cults_war_god_name
		WarGodNamePossessive = bulwari_sun_cults_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = bulwari_sun_cults_trickster_god_name
		TricksterGodNamePossessive = bulwari_sun_cults_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = bulwari_sun_cults_night_god_name
		NightGodNamePossessive = bulwari_sun_cults_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = bulwari_sun_cults_water_god_name
		WaterGodNamePossessive = bulwari_sun_cults_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM


		PantheonTerm = bulwari_sun_cults_high_god_name
		PantheonTerm2 = bulwari_sun_cults_high_god_name
		PantheonTerm3 = bulwari_sun_cults_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			bulwari_sun_cults_high_god_name
			bulwari_sun_cults_high_god_name_alternate
			bulwari_sun_cults_lord_of_light
		}
		
		DevilName = bulwari_sun_cults_devil_name
		DevilNamePossessive = bulwari_sun_cults_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_IT
		DevilHerHis = CHARACTER_HERHIS_ITS
		DevilHerselfHimself = bulwari_sun_cults_devil_herselfhimself
		EvilGodNames = {
			bulwari_sun_cults_devil_name
			bulwari_sun_cults_evil_god_malevolent_dark
			bulwari_sun_cults_evil_god_rasa
			bulwari_sun_cults_evil_god_jiqaat
			bulwari_sun_cults_evil_god_ukedu
			bulwari_sun_cults_evil_god_irhungal
			bulwari_sun_cults_evil_god_wanamur
			bulwari_sun_cults_evil_god_niksaru
			bulwari_sun_cults_evil_god_saksu
			bulwari_sun_cults_evil_god_darkness_evils
		}
		HouseOfWorship = bulwari_sun_cults_house_of_worship
		HouseOfWorship2 = bulwari_sun_cults_house_of_worship
		HouseOfWorship3 = bulwari_sun_cults_house_of_worship
		HouseOfWorshipPlural = bulwari_sun_cults_house_of_worship_plural
		ReligiousSymbol = bulwari_sun_cults_religious_symbol
		ReligiousSymbol2 = bulwari_sun_cults_religious_symbol
		ReligiousSymbol3 = bulwari_sun_cults_religious_symbol
		ReligiousText = bulwari_sun_cults_religious_text
		ReligiousText2 = bulwari_sun_cults_religious_text
		ReligiousText3 = bulwari_sun_cults_religious_text
		ReligiousHeadName = bulwari_sun_cults_religious_head_title
		ReligiousHeadTitleName = bulwari_sun_cults_religious_head_title_name
		DevoteeMale = bulwari_sun_cults_devotee_male
		DevoteeMalePlural = bulwari_sun_cults_devotee_male_plural
		DevoteeFemale = bulwari_sun_cults_devotee_female
		DevoteeFemalePlural = bulwari_sun_cults_devotee_female_plural
		DevoteeNeuter = bulwari_sun_cults_devotee_neuter
		DevoteeNeuterPlural = bulwari_sun_cults_devotee_neuter_plural
		PriestMale = bulwari_sun_cults_priest
		PriestMalePlural = bulwari_sun_cults_priest_plural
		PriestFemale = bulwari_sun_cults_priest
		PriestFemalePlural = bulwari_sun_cults_priest_plural
		PriestNeuter = bulwari_sun_cults_priest
		PriestNeuterPlural = bulwari_sun_cults_priest_plural
		AltPriestTermPlural = bulwari_sun_cults_priest_term_plural
		BishopMale = bulwari_sun_cults_bishop
		BishopMalePlural = bulwari_sun_cults_bishop_plural
		BishopFemale = bulwari_sun_cults_bishop
		BishopFemalePlural = bulwari_sun_cults_bishop_plural
		BishopNeuter = bulwari_sun_cults_bishop
		BishopNeuterPlural = bulwari_sun_cults_bishop_plural
		DivineRealm = bulwari_sun_cults_positive_afterlife
		DivineRealm2 = bulwari_sun_cults_positive_afterlife
		DivineRealm3 = bulwari_sun_cults_positive_afterlife
		PositiveAfterLife = bulwari_sun_cults_positive_afterlife
		PositiveAfterLife2 = bulwari_sun_cults_positive_afterlife
		PositiveAfterLife3 = bulwari_sun_cults_positive_afterlife
		NegativeAfterLife = bulwari_sun_cults_negative_afterlife
		NegativeAfterLife2 = bulwari_sun_cults_negative_afterlife
		NegativeAfterLife3 = bulwari_sun_cults_negative_afterlife
		DeathDeityName = bulwari_sun_cults_death_name
		DeathDeityNamePossessive = bulwari_sun_cults_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		DeathDeityHerHim = CHARACTER_HERHIM_HIM

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		jaherian_cults = {
			color = { 255 201 14 }
			icon = bulwari_sun_cult

			holy_site = bulwar
			holy_site = eduz_vacyn
			holy_site = azka_sur
			holy_site = azkaszelazka
			holy_site = brasan

			doctrine = tenet_unrelenting_faith
			doctrine = tenet_ritual_hospitality
			doctrine = tenet_suraels_rebirth

			doctrine = doctrine_clerical_function_recruitment
			
			#Main Group
			doctrine = doctrine_temporal_head # Jaher at start but if possible it should be switched to no one if he dies and the sun elves fail to stabilise
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment

			localization = {
				#HighGod - Surael
				HighGodName = jaherian_cults_high_god_name
				HighGodName2 = jaherian_cults_high_god_name
				HighGodNamePossessive = jaherian_cults_high_god_name_possessive

				#Creator
				CreatorName = jaherian_cults_creator_god_name
				CreatorNamePossessive = jaherian_cults_creator_god_name_possessive

				#HealthGod
				HealthGodName = jaherian_cults_health_god_name
				HealthGodNamePossessive = jaherian_cults_health_god_name_possessive
				
				#FertilityGod
				FertilityGodName = jaherian_cults_fertility_god_name
				FertilityGodNamePossessive = jaherian_cults_fertility_god_name_possessive

				#WealthGod
				WealthGodName = jaherian_cults_wealth_god_name
				WealthGodNamePossessive = jaherian_cults_wealth_god_name_possessive

				#HouseholdGod
				HouseholdGodName = jaherian_cults_household_god_name
				HouseholdGodNamePossessive = jaherian_cults_household_god_name_possessive

				#FateGod
				FateGodName = jaherian_cults_fate_god_name
				FateGodNamePossessive = jaherian_cults_fate_god_name_possessive

				#KnowledgeGod
				KnowledgeGodName = jaherian_cults_knowledge_god_name
				KnowledgeGodNamePossessive = jaherian_cults_knowledge_god_name_possessive

				#WarGod
				WarGodName = jaherian_cults_war_god_name
				WarGodNamePossessive = jaherian_cults_war_god_name_possessive

				#TricksterGod
				TricksterGodName = jaherian_cults_trickster_god_name
				TricksterGodNamePossessive = jaherian_cults_trickster_god_name_possessive

				#NightGod
				NightGodName = jaherian_cults_night_god_name
				NightGodNamePossessive = jaherian_cults_night_god_name_possessive

				#WaterGod
				WaterGodName = jaherian_cults_water_god_name
				WaterGodNamePossessive = jaherian_cults_water_god_name_possessive

				PantheonTerm = jaherian_cults_high_god_name
			}
		}

		way_of_kings = { 
			color =  { 71 89 149 }
			icon = old_bulwari_sun_cult

			holy_site = tazh_qel_araksa
			holy_site = eduz_vacyn
			holy_site = bulwar
			holy_site = azka_sur
			holy_site = brasan

			doctrine = tenet_pursuit_of_power
			doctrine = tenet_surael_worship
			doctrine = tenet_mystical_birthright

			doctrine = doctrine_clerical_function_recruitment
		}
		
		cult_of_the_eclipse = {
			color = { 255 201 14 }
			icon = old_bulwari_sun_cult

			holy_site = bulwar
			holy_site = azkabar
			holy_site = eduz_vacyn
			holy_site = azkaszelazka
			holy_site = brasan
			
			doctrine = tenet_false_conversion_sanction
			doctrine = tenet_surael_worship

		}
		
		cult_of_kuza = {
			color = { 55 65 100 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_satkuza
			holy_site = eduz_vacyn
			holy_site = eduz_szel_ninezim
			holy_site = bulwar
			holy_site = brasan

			doctrine = tenet_communal_identity
			doctrine = tenet_kuza_worship
			doctrine = tenet_esotericism

			doctrine = doctrine_funeral_sky_burial
			doctrine = doctrine_clerical_function_alms_and_pacification
		}

		cult_of_the_citadel = {
			color =  { 178 40 138 }
			icon = old_bulwari_sun_cult

			holy_site = birsartansbar
			holy_site = eduz_vacyn
			holy_site = azkaszelazka
			holy_site = mount_lazzaward
			holy_site = aqatbar

			doctrine = tenet_unrelenting_faith
			doctrine = tenet_surael_worship
			doctrine = tenet_armed_pilgrimages

			doctrine = doctrine_homosexuality_crime
			doctrine = doctrine_clerical_function_recruitment
		}

		rite_of_birsartan = {
			color =  { 126 213 89 }
			icon = old_bulwari_sun_cult

			holy_site = birsartansbar
			holy_site = eduz_vacyn
			holy_site = aqatbar
			holy_site = mount_lazzaward
			holy_site = eduz_szel_ninezim

			doctrine = tenet_pentarchy
			doctrine = tenet_surael_worship
			doctrine = tenet_esotericism

			doctrine = doctrine_homosexuality_crime
		}

		cult_of_the_eseral_mitellu = {
			color =  { 80 205 25 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_vacyn
			holy_site = puhiya
			holy_site = eduz_szel_ninezim
			holy_site = mount_lazzaward
			holy_site = azkabar

			doctrine = tenet_legalism
			doctrine = tenet_surael_worship
			doctrine = tenet_bhakti

			doctrine = doctrine_bastardry_all
			doctrine = doctrine_homosexuality_crime
		}

		way_of_the_sea = {
			color = { 237 31 61 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_satkuza
			holy_site = eduz_vacyn
			holy_site = puhiya
			holy_site = aqatbar
			holy_site = brasan

			doctrine = tenet_bhakti
			doctrine = tenet_surael_worship
			doctrine = tenet_ritual_celebrations

			doctrine = doctrine_clerical_function_alms_and_pacification
		}

		rite_of_the_eternal_home = {
			color =  { 205 233 232 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_satkuza
			holy_site = eduz_vacyn
			holy_site = puhiya
			holy_site = ekluzagnu
			holy_site = brasan

			doctrine = tenet_pentarchy
			doctrine = tenet_surael_worship
			doctrine = tenet_ritual_hospitality

			doctrine = doctrine_homosexuality_crime
			doctrine = doctrine_witchcraft_crime
			doctrine = doctrine_pluralism_righteous
			doctrine = doctrine_pilgrimage_mandatory
			doctrine = doctrine_spiritual_head
		}

		cult_of_mourning = {
			color =  { 240 200 95 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_satkuza
			holy_site = eduz_vacyn
			holy_site = duklum_tanuz
			holy_site = bulwar
			holy_site = brasan

			doctrine = tenet_unrelenting_faith
			doctrine = tenet_surael_worship
			doctrine = tenet_bhakti

			doctrine = doctrine_clerical_function_alms_and_pacification
		}

		rite_of_the_dancing_fire = {
			color =  { 213 98 117 }
			icon = old_bulwari_sun_cult

			holy_site = tazh_qel_araksa
			holy_site = eduz_vacyn
			holy_site = azkabar
			holy_site = bulwar
			holy_site = mount_lazzaward

			doctrine = tenet_pentarchy
			doctrine = tenet_surael_worship
			doctrine = tenet_ritual_hospitality

			doctrine = doctrine_homosexuality_crime
			doctrine = doctrine_witchcraft_crime
			doctrine = doctrine_pluralism_righteous
			doctrine = doctrine_pilgrimage_mandatory
			doctrine = doctrine_spiritual_head
		}

		cult_of_the_stone = {
			color =  { 178 163 122 }
			icon = old_bulwari_sun_cult

			holy_site = eduz_satkuza
			holy_site = eduz_vacyn
			holy_site = ekluzagnu
			holy_site = puhiya
			holy_site = brasan

			doctrine = tenet_cthonic_redoubts
			doctrine = tenet_surael_worship
			doctrine = tenet_bhakti

			doctrine = doctrine_clerical_function_recruitment
		}

		cult_of_the_law = {
			color =  { 194 254 117 }
			icon = old_bulwari_sun_cult

			holy_site = azkabar
			holy_site = eduz_vacyn
			holy_site = azka_sur
			holy_site = bulwar
			holy_site = brasan

			doctrine = tenet_legalism
			doctrine = tenet_surael_worship
			doctrine = tenet_mendicant_preachers

			doctrine = doctrine_divorce_approval
			doctrine = doctrine_bastardry_all
			doctrine = doctrine_pluralism_righteous
		}

		rite_of_hammura = {
			color =  { 113 232 24 }
			icon = old_bulwari_sun_cult

			holy_site = azka_sur
			holy_site = eduz_vacyn
			holy_site = azkabar
			holy_site = ebbusubtu
			holy_site = aqatbar

			doctrine = tenet_pentarchy
			doctrine = tenet_surael_worship
			doctrine = tenet_religious_legal_pronouncements

			doctrine = doctrine_witchcraft_crime
			doctrine = doctrine_pluralism_righteous
			doctrine = doctrine_pilgrimage_mandatory
			doctrine = doctrine_spiritual_head
		}

		cult_of_the_wise_master = {
			color =  { 54 184 198 }
			icon = old_bulwari_sun_cult

			holy_site = mount_lazzaward
			holy_site = eduz_vacyn
			holy_site = aqatbar
			holy_site = azkabar
			holy_site = birsartansbar

			doctrine = tenet_bhakti
			doctrine = tenet_surael_worship
			doctrine = tenet_pastoral_isolation
			
			doctrine = special_faith_harpy_tolerance #So they don't constantly rebel against Harpylen

			doctrine = doctrine_clerical_function_alms_and_pacification
		}

		cult_of_the_sands = {
			color =  { 200 105 50 }
			icon = old_bulwari_sun_cult

			holy_site = azka_sur
			holy_site = eduz_vacyn
			holy_site = ebbusubtu
			holy_site = azka_szel_udam
			holy_site = edesukeru

			doctrine = tenet_ritual_hospitality
			doctrine = tenet_surael_worship
			doctrine = tenet_communal_identity

			doctrine = doctrine_clerical_function_alms_and_pacification
		}

		cult_cult_of_maqet = {
			color =  { 190 190 50 }
			icon = old_bulwari_sun_cult

			holy_site = azka_szel_udam
			holy_site = eduz_vacyn
			holy_site = ekluzagnu
			holy_site = bulwar
			holy_site = brasan

			doctrine = tenet_unrelenting_faith
			doctrine = tenet_surael_worship
			doctrine = tenet_communal_identity
		}
	}
}