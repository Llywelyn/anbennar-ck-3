k_vrorenmarch = {
	1000.1.1 = { change_development_level = 7 }
}

d_vrorenwall = {
	1020.10.31 = {
		holder = 78 #Harolt Vrorensson
		government = feudal_government
	}
}

c_mammoth_hall = {
	1000.1.1 = { change_development_level = 11 }
	1020.10.31 = {
		holder = 78 #Harolt Vrorensson
	}
}

c_bal_vroren = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 78 #Harolt Vrorensson
	}
}

c_gulleton = {
	1020.10.31 = {
		holder = 78 #Harolt Vrorensson
	}
}

c_serpentswall = {
	1020.10.31 = {
		holder = 78 #Harolt Vrorensson
	}
}

d_ebonmarck = {
	1020.10.31 = {
		holder = 79 #Garrec Ebonfrost
		government = feudal_government
	}
}

c_ebonham = {
	1000.1.1 = { change_development_level = 8 }
}

c_tencfor = {
	1022.1.1 = { change_development_level = 9 }
	
	1020.11.1 = {
		liege = "d_ebonmarck"
		holder = 522 #Carleon the Inadequate, son of Venac the Arrogant
	}
}

c_gerudswatch = {
	1022.1.1 = { change_development_level = 6 }
	
	1020.11.1 = {
		liege = "d_ebonmarck"
		holder = 522 #Carleon the Inadequate, son of Venac the Arrogant
	}
}

c_cedevik = {
	1000.1.1 = { change_development_level = 9 }
	1020.10.31 = {
		holder = 80 #Carl Alcarsson
		government = feudal_government
	}
}

c_coldwood = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 80 #Carl Alcarsson
		government = feudal_government
	}
}

c_rivsby = {
	1000.1.1 = { change_development_level = 9 }
	1020.10.31 = {
		holder = 81 #Ivan Alcarsson
		government = feudal_government
	}
}

c_vertwic = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 81 #Ivan Alcarsson
		government = feudal_government
	}
}

c_sondaar = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.31 = {
		holder = 82 #Victor of Sondaar
		government = republic_government
	}
}

c_eskerborg = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 83 #Wilford of Eskerborg
		government = republic_government
	}
}

d_wudhal = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 84 #Marc Aldwoud
	}
}

c_wudhal = {
	1020.10.31 = {
		holder = 84 #Marc Aldwoud
	}
}

c_tannusvale = {
	1020.10.31 = {
		holder = 84 #Marc Aldwoud
	}
}

c_esald = {
	1000.1.1 = { change_development_level = 9 }
	1020.10.31 = {
		holder = 85 #Allec of Esald
		government = republic_government
	}
}

c_estshore = {
	1020.10.31 = {
		holder = 85 #Allec of Esald
		government = republic_government
	}
}

c_whitwic = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.31 = {
		holder = 86 #Eric of Whitwic
		government = republic_government
	}
}

c_chillpoint = {
	1020.10.31 = {
		holder = 87 #Maren of Chillpoint
		government = republic_government
	}
}

c_three_giants = {
	1020.10.31 = {
		holder = 87 #Maren of Chillpoint
		government = republic_government
	}
}

c_giantswood = {
	958.1.1 = {
		holder = alderwood_0001
	}
	1015.12.12 = {
		holder = alderwood_0002
	}
	1020.11.1 = {
		liege = "d_ebonmarck"
		holder = alderwood_0002
	}
}

c_giantsfort = {
	958.1.1 = {
		holder = alderwood_0001
	}
	1015.12.12 = {
		holder = alderwood_0002
	}
	1020.11.1 = {
		liege = "d_ebonmarck"
		holder = alderwood_0002
	}
}

