﻿
k_brasan = {
	993.1.1 = {
		government = republic_government
		holder = bulwari0006 # Asdar szel-Brasan
	}
	1006.6.22 = {
		liege = e_bulwar
	}
}

c_brasan = {
	1000.1.1 = { change_development_level = 22 }
	993.1.1 = {
		holder = bulwari0006 # Asdar szel-Brasan
	}
}

c_djinnkalis = {
	1000.1.1 = { change_development_level = 11 }
}

c_rakkubtu = {
	1000.1.1 = { change_development_level = 12 }
}

c_brasan_tanuz = {
	1000.1.1 = { change_development_level = 10 }
}

c_kadumar = {
	1000.1.1 = { change_development_level = 10 }
}

c_suran_narit = {
	1000.1.1 = { change_development_level = 12 }
}

c_urasum = {
	1000.1.1 = { change_development_level = 13 }
}

c_aqatabtu = {
	1000.1.1 = { change_development_level = 12 }
}